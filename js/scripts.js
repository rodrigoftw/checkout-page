let item1 = 1;
let item2 = 1;

const addButton1 = document.getElementById("add-button-1");
const removeButton1 = document.getElementById("remove-button-1");

const addButton2 = document.getElementById("add-button-2");
const removeButton2 = document.getElementById("remove-button-2");

const itemCount1 = document.getElementById("item-count-1");
const itemCount2 = document.getElementById("item-count-2");

itemCount1.innerHTML = item1;
itemCount2.innerHTML = item2;

var addFunction1 = () => item1 += 1;
var removeFunction1 = () => {
  return item1 > 1 ? item1 -= 1 : item1 = 0;
};

var addFunction2 = () => item2 += 1;
var removeFunction2 = () => {
  return item2 > 1 ? item2 -= 1 : item2 = 0;
};

function add1() { itemCount1.innerHTML = addFunction1(); }

function remove1() { itemCount1.innerHTML = removeFunction1(); }

function add2() { itemCount2.innerHTML = addFunction2(); }

function remove2() { itemCount2.innerHTML = removeFunction2(); }

const submitButton = document.getElementById("submit-button");

const emailElement = document.getElementById("email");
const phoneElement = document.getElementById("phone");
const nameElement = document.getElementById("name");
const addressElement = document.getElementById("address");
const cityElement = document.getElementById("city");
const countryElement = document.getElementById("country");
const postalCodeElement = document.getElementById("postal-code");

function submitForm() {
  if (emailElement.checkValidity() &&
    phoneElement.checkValidity() &&
    nameElement.checkValidity() &&
    addressElement.checkValidity() &&
    cityElement.checkValidity() &&
    countryElement.checkValidity() &&
    postalCodeElement.checkValidity()) {
    alert("Nice purchase! Your items are being carefully packed.");
  }
}