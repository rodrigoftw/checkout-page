# Checkout Page #

## Solution for a challenge from [Devchallenges.io](http://devchallenges.io). ##


## [Demo](https://vibrant-newton-503f09.netlify.app/) | [Solution](https://devchallenges.io/solutions/HMTxIARN4QpN2Tds3VE0) | [Challenge](https://devchallenges.io/challenges/0J1NxxGhOUYVqihwegfO) ##

## Table of Contents

- [Overview](#overview)
- [Built With](#built-with)
- [Features](#features)
- [Contact](#contact)

## Overview

![screenshot](https://devchallenges.io/_next/image?url=https%3A%2F%2Ffirebasestorage.googleapis.com%2Fv0%2Fb%2Fdevchallenges-1234.appspot.com%2Fo%2FchallengesDesigns%252FCheckoutThumbnail.png%3Falt%3Dmedia%26token%3Dc7ffdbe3-7206-44f2-b1e6-a6b99bf81901&w=1920&q=75)

This application/site was created as a submission to a [DevChallenges](https://devchallenges.io/challenges) challenge.
The [challenge](https://devchallenges.io/challenges/0J1NxxGhOUYVqihwegfO) was to build an application to complete the given user stories.

## Built With

- [HTML](https://html.spec.whatwg.org/)
- [CSS](https://www.w3.org/Style/CSS/Overview.en.html)
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

## Features

This project features a responsive checkout page that goes from a small mobile (320px width) view up to a desktop (4K) view.

## Contact

You can contact me [here](https://linktr.ee/rodrigodev).
